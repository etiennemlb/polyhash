from . import methods
from . import utils


def getMethodsList():
    return methods.methods


def solve(files, method):
    """[summary]
        Solve one or more files, output the solution to a file

    Arguments:
        files {str} -- The list of files that will be processed
        method {str} -- The method used to solve the problem (default: {'naive'})
    """

    for file in files:
        model = methods.methodFactory(method, file)
        print('Initializing')
        model.init()
        print('Solving')
        model.solve()

        serializer = utils.serializer(file, method)

        print('Getting results')
        model.get_result(serializer)
        print('Writing results to disk')
        serializer.serialize()


def solve_and_count(files, method):
    """[summary]
        Solve one or more files, output the solution to a file and count the 
        amount of point
    Arguments:
        files {str} -- The list of files that will be processed
        method {str} -- The method used to solve the problem (default: {'naive'})
    """
    for file in files:
        #for i in range(74, 80):
        model = methods.methodFactory(method, file)
        print('Initializing')
        model.init()

        print('Solving')        
        model.solve()

        serializer = utils.serializer(file, method) # str(i)

        print('Getting results')
        model.get_result(serializer)
        print('Writing results to disk')
        serializer.serialize()

        print('Counting points')
        count_points(file, method, model.tokenizer)


def count_points(file, method, tokenized_input=None):
    """[summary]
        Compute the points earned for a given file and method

    Arguments:
        file {str} -- The file path representing the file to be processed, it must be a *.in file
                      The *.out representing the output of an algorithm will be deduced from the *.in

    Keyword Arguments:
        tokenized_input {utils.tokenizer} -- The tokenizer representing the *.in file, must be in a tokenized state (default: {None})
    """

    if tokenized_input is None:
        tokenized_input = utils.tokenizer(file)
        tokenized_input.tokenize()

    output_file = file + '.' + method + '.out'
    counter = utils.point_counter(tokenized_input, output_file)
    points = counter.compute()
    print('Won [', points, '] points out of', tokenized_input.get_max_score(), '(without bonus) or', float(
        points)/float(tokenized_input.max_score_without_bonus)*100, '%', output_file)
