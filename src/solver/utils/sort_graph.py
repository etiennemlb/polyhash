from itertools import combinations


class Sort():
    def __init__(self, louis_method, sort_type, argument=None):
        self.method = louis_method
        self.arg = argument  # > or <

        if sort_type == 'remove_extra':
            self.remove_extra()

        if sort_type == 'equilibrage':
            self.balancing()

        if sort_type == 'ratio':
            self.function = self.ratio
            self.quicksort_ratio()

        elif sort_type == 'not_worth':
            self.not_worth()

        elif sort_type == 'dispatch':
            self.dispatch()

    def comp(self, a, b):
        """
        Funtion of comparaison
        """
        if self.arg == '>':
            return a >= b
        elif self.arg == '<':
            return a <= b

    def remove_extra(self):
        """
        Used to determine how much connex composant can exist
        It mean that we remove connex graph that cannot be upload on serv because of the use of
        servers by other connex graph
        This method will pop connex graph that have a (self compile time + compile time of previous connex graph)
        greater than their deadline
        """
        # We assume that one connex graph have all the servers
        total_compile = 0
        i = 0
        while i < len(self.method.graph[1]):
            liste = self.method.vector_info_graph[i]
            total_compile += (liste[0]/self.method.tokenizer.serverCount)
            # total_compile > deadline branche
            if total_compile > self.method.vector_info_graph[i][2][0]:
                self.method.graph[1].pop(i)
                self.method.vector_info_graph.pop(i)
                i -= 1
            i += 1


##################
# SORT RATIO
##################

    def __quick_ratio(self, liste_branche, debut, fin, indice_pivot):
        if debut < fin:
            indice = self.partition(liste_branche, debut, fin, indice_pivot)

            self.__quick_ratio(liste_branche, debut, indice-1, indice_pivot)
            self.__quick_ratio(liste_branche, indice+1, fin, indice+1)

        return liste_branche

    def quicksort_ratio(self):
        """
        Sort the graph on the ratio of each connex graph
        """
        self.__quick_ratio(
            self.method.graph[1], 0, len(self.method.graph[1])-1, 0)

    def ratio(self, indice):
        """
        :return: ratio entre le score et le temps de compilation de la branche d'indice i
        :rtype: float
        """
        return 1/(self.method.vector_info_graph[indice][2][1]/self.method.vector_info_graph[indice][2][0])

##################
# SORT COMPILE
##################

    def __quick_compile_dispatch(self, liste_branche, debut, fin, indice_pivot):
        if debut < fin:
            indice = self.partition(liste_branche, debut, fin, indice_pivot)

            self.__quick_compile_dispatch(
                liste_branche, debut, indice-1, indice_pivot)
            self.__quick_compile_dispatch(
                liste_branche, indice+1, fin, indice+1)

        return liste_branche

    def quicksort_compile_dispatch(self, indice_branche):
        """
        Sort the graph on the compile time
        Used for the dispatch method
        """
        self.function = self.compile_time_file
        self.arg = '<'
        self.indice_branche_dispatch = indice_branche
        branche = self.method.graph[1][indice_branche]
        self.__quick_compile_dispatch(branche[1], 0, len(branche[1])-1, 0)

    def compile_time_file(self, name):
        """
        :return: Return compile time of the file 'name'
        :rtype: int
        """
        return self.method.tokenizer.info_about(name)[0]


    def sort_by_compile(self, liste_files):
        """
        Used to sort a list of files by their compile time
        """
        def __sort_by_compile(elem):

            return self.method.tokenizer.info_about(elem[0])[0]

        liste_files.sort(key=__sort_by_compile)


##################
# OTHERS
##################

    def not_worth(self):
        """
        Remove not worth files from the graph
        """
        i = 0
        while i < len(self.method.graph[1]):
            liste = self.method.vector_info_graph[i]
            if liste[0] > liste[2][0]:
                # If compile time > Deadline Target
                if (liste[0]/(self.method.tokenizer.serverCount)) + liste[2][2]+2 > liste[2][0]:
                    # If (compile_time_connex_graph/nb_serv) + compile_time_target + 2  > Replication_time_target
                    if len(self.method.graph[1]) != 1:
                        # Then, delete the connex graph
                        self.method.graph[1].pop(i)
                        self.method.vector_info_graph.pop(i)
                        i -= 1
            i += 1

    def find_combination(self, l, nb_elem, sum_nb):
        """
        Find a combination of n element (nb_elem) in l where the sum of compile is equal to sum_nb

        :param: List of compile time and server
        :nb_elem: number of elem in the combination
        :sum_nb: must be equal to this
        """
        for n in range(1, len(l) + 1):
            for combination in combinations(l, nb_elem):
                sum_compile = 0
                for file in combination:
                    sum_compile += self.method.tokenizer.info_about(file[0])[0]
                if sum_compile == sum_nb:
                    return combination

    def remove_after_comb(self, l, res_combi):
        """
        After having the combination with the wanted sum and number,
        we have to remove it from the list 
        """
        for x in res_combi:
            i = 0
            for y in l:
                if x == y:
                    l.pop(i)
                i += 1

    def where_to_put(self, l, sum_nb):
        """
        Method to know in wich `free serv used by a serv` the files need to go
        """
        res = []
        i = 1
        while len(l) > self.vector_nb_repartition[0]:
            res_combi = list(self.find_combination(l, self.vector_nb_repartition[i], sum_nb))
            res.append(res_combi)
            self.remove_after_comb(l, res_combi)
            i += 1

        res.append(l)
        return res

    def dispatch(self):
        """
        Function used to dispatch connex graph into free servers
        """
        #self.method.free_serv[0] : nb free servers
        #self.method.free_serv[1] : separation index between used and free serv = nb used 
        #self.method.free_serv[2] : nb total servers

        j = self.method.free_serv[1] # we go to the separation
        nb_removed_free_serv = 0
        self.link_between_used_free = {} # map used to see link between used serv with their free serv 

        if self.method.free_serv[1]/self.method.free_serv[0] <= 1:
            # nb_used / nb_free
            if self.method.free_serv[0] % 2 != 0:
                # nb_free is not even
                self.method.free_serv[0] -= 1
            nb_free_per_used = int(self.method.free_serv[0] / self.method.free_serv[1])
            # number of free servers for each used server

            for k in range(self.method.free_serv[1]):
                #for all connex graph to the separation index

                # add link between used and free servers
                connex_graph = self.method.graph[1][k]
                self.max_time_compile = int(self.method.vector_info_graph[k][0]/(nb_free_per_used+1))
                self.link_between_used_free[k] = []
                for i in range(int(nb_free_per_used)):
                    self.link_between_used_free[k].append(k+self.method.free_serv[1]+i)

                # sort on compilation time
                self.quicksort_compile_dispatch(k)

                if j == self.method.free_serv[2]:
                    # total serv
                    j = self.method.free_serv[1]
                    # go back to separation index

                if len(connex_graph[1]) % 2 == 0:
                    nb_to_dispatch = int(
                        len(connex_graph[1]) * self.method.coeff_dispatch)
                else:
                    nb_to_dispatch = int(
                        len(connex_graph[1]) * self.method.coeff_dispatch)-1

                if nb_free_per_used == 0:
                    nb_free_per_used += 1
                nb_to_dispatch_per_serv = nb_to_dispatch / nb_free_per_used


                # Creation of a vector inside wich is stocked the number of file to dispatch per serv
                difference = int(len(connex_graph[1]) - nb_to_dispatch_per_serv*((nb_free_per_used)+1))
                reste = int(self.method.free_serv[2]-difference)
                self.vector_nb_repartition = []
                for i in range(difference):
                    self.vector_nb_repartition.append(int(nb_to_dispatch_per_serv+1))
                for i in range(reste):
                    self.vector_nb_repartition.append(
                        int(nb_to_dispatch_per_serv))

                
                # nb_to_dispatch_pre_serv = [nb dispatch actual serv, serv(j+1), serv(j+2), serv(j+3) ..., serv(j+nb_free_per_used)]
                # We count the deadline needed for each serv 
                # with serv(i)∈{not used servers | serv(i) usable by actual server}

                for m in range(int(nb_free_per_used)):
                    j += m
                    # filling free serv
                    self.method.graph[1].append(['#', []])

                    # new serv in vector graph
                    self.method.vector_info_graph.append([0, 0, [0, 0, 0, 0]])
                    nb_removed_free_serv += 1


                    # In this part we take files from used serv to free serv
                    indice_to_remove = 1
                    while indice_to_remove <= len(connex_graph[1])-1:
                        # for i in range(int(nb_to_dispatch_per_serv)):
                        if self.method.vector_info_graph[k][2][0] > self.method.vector_info_graph[k][1]/self.method.tokenizer.serverCount:
                            # temps compil targ            >               temps repl/nb serv
                            info_about_file_to_remove = self.method.tokenizer.info_about(connex_graph[1][indice_to_remove][0])

                            self.method.graph[1][-1][1].append(connex_graph[1].pop(indice_to_remove))

                            self.method.vector_info_graph[k][0] -= info_about_file_to_remove[0]
                            self.method.vector_info_graph[k][1] -= info_about_file_to_remove[1]

                            self.method.vector_info_graph[-1][0] += info_about_file_to_remove[0]
                            self.method.vector_info_graph[-1][1] += info_about_file_to_remove[1]

                            indice_to_remove += int(nb_free_per_used) - m

            self.method.free_serv[0] -= nb_removed_free_serv
            self.method.free_serv[1] = len(self.method.graph[1])

            if not self.test_validity():  # see test_validity method
                print('  balancing')
                self.balancing()  # Have a close solution to the problem : 
                                    # each serv can achieve the compilation of the target, but they miss them by a few time
                print('  done')

                for indice in self.link_between_used_free[k]:
                    self.sort_by_compile(self.method.graph[1][indice][1])

                temp_list = []
                for serv in self.link_between_used_free[k]:
                    temp_list += (self.method.graph[1][serv][1])
                temp_list += self.method.graph[1][k][1]

                # Making combinations on the list
                print('  searching combinations : a bit long ~ 3min for e_intriguing')
                res_final = self.where_to_put(
                    temp_list, self.max_time_compile)
                print('  done')

                for serv in self.link_between_used_free[k]:
                    branch = res_final.pop(0)
                    self.method.graph[1][serv][1] = branch

                self.method.graph[1][k][1] = res_final.pop(0)

    def test_validity(self):
        """
        Method used to know if a balancing is needed

        :return: True if a balancing is needed, False otherwise
        :rtype: bool
        """
        for indice_branche in self.link_between_used_free:
            deadline_minus_initial_compil_time = (
                    self.method.vector_info_graph[indice_branche][2][0] - self.method.vector_info_graph[indice_branche][2][2] - 1)

            if self.method.vector_info_graph[indice_branche][0] > deadline_minus_initial_compil_time:
                return False
            for serv in self.link_between_used_free[indice_branche]:
                if self.method.vector_info_graph[serv][0] > deadline_minus_initial_compil_time:
                    return False

        return True

    def balancing(self):
        """
        This method is used to balance the graph in order to have a close solution 
        It make the combination easier & faster
        """
        for indice_branche in self.link_between_used_free:
            deadline_minus_compil_time = {}
            stop = False

            deadline_minus_initial_compil_time = self.method.vector_info_graph[indice_branche][
                2][0] - self.method.vector_info_graph[indice_branche][2][2]

            deadline_minus_compil_time[indice_branche] = deadline_minus_initial_compil_time

            for i in self.link_between_used_free[indice_branche]:
                deadline_minus_compil_time[i] = deadline_minus_initial_compil_time

            max_iteration = 0
            while not stop and max_iteration < 2000:

                serveur_max = indice_branche  # We have to determine wich serv exceeds the most
                for i in self.link_between_used_free[indice_branche]:
                    if self.method.vector_info_graph[i][0] > self.method.vector_info_graph[serveur_max][0]:
                        serveur_max = i

                depassement_serv_max = self.method.vector_info_graph[serveur_max][0] - deadline_minus_compil_time[serveur_max]

                if depassement_serv_max > 0:  # If deadline is outdated
                    serveur_min = indice_branche
                    for i in self.link_between_used_free[indice_branche]:
                        if self.method.vector_info_graph[i][0] <= self.method.vector_info_graph[serveur_min][0]:
                            serveur_min = i

                    dispo_serv_min = deadline_minus_compil_time[serveur_min] - self.method.vector_info_graph[serveur_min][0]

                    # Need to find file in interval
                    j = 0
                    found_max = False
                    while not found_max and j < len(self.method.graph[1][serveur_max][1]):
                        file_max = self.method.graph[1][serveur_max][1][j][0]

                        if depassement_serv_max <= self.method.tokenizer.info_about(file_max)[0]:

                            # File is put in minimal serv
                            depassement = self.method.tokenizer.info_about(file_max)[0] - \
                                dispo_serv_min
                            self.method.graph[1][serveur_min][1].append(
                                self.method.graph[1][serveur_max][1].pop(j))

                            # Update vector info
                            self.method.vector_info_graph[serveur_max][0] -= self.method.tokenizer.info_about(file_max)[0]
                            self.method.vector_info_graph[serveur_min][0] += self.method.tokenizer.info_about(file_max)[0]

                            k = 0
                            found_min = False
                            while not found_min and k < len(self.method.graph[1][serveur_min][1]):
                                file_min = self.method.graph[1][serveur_min][1][k][0]

                                if depassement <= self.method.tokenizer.info_about(file_min)[0]:
                                    self.method.graph[1][serveur_max][1].append(self.method.graph[1][serveur_min][1].pop(k))

                                    # Update vector info
                                    self.method.vector_info_graph[serveur_max][0] += self.method.tokenizer.info_about(file_min)[0]
                                    self.method.vector_info_graph[serveur_min][0] -= self.method.tokenizer.info_about(file_min)[0]

                                    found_min = True
                                k += 1
                            found_max = True
                        j += 1


                finish = True
                if self.method.vector_info_graph[indice_branche][0] > deadline_minus_compil_time[indice_branche]:
                    finish = False
                else:
                    for i in self.link_between_used_free[indice_branche]:
                        if self.method.vector_info_graph[i][0] > deadline_minus_compil_time[i]:
                            finish = False
                if finish:
                    stop = True
                max_iteration += 1

   
    def partition(self, liste_branche, g, d, indice_pivot):
        if self.function == self.compile_time_file:

            if g <= d:
                g_treat = self.function(liste_branche[g][0])
                piv_treat = self.function(liste_branche[indice_pivot][0])
                if self.comp(g_treat, piv_treat):
                    g += 1

                else:
                    liste_branche[g], liste_branche[d] = liste_branche[d], liste_branche[g]
                    d -= 1

                return self.partition(liste_branche, g, d, indice_pivot)

            else:
                liste_branche[indice_pivot], liste_branche[d] = liste_branche[d], liste_branche[indice_pivot]
                return d
        else:
            if g <= d:
                g_treat = self.function(g)
                piv_treat = self.function(indice_pivot)
                if self.comp(g_treat, piv_treat):
                    g += 1

                else:
                    liste_branche[g], liste_branche[d] = liste_branche[d], liste_branche[g]
                    self.method.vector_info_graph[g], self.method.vector_info_graph[
                        d] = self.method.vector_info_graph[d], self.method.vector_info_graph[g]
                    d -= 1

                return self.partition(liste_branche, g, d, indice_pivot)

            else:
                self.method.vector_info_graph[indice_pivot], self.method.vector_info_graph[
                    d] = self.method.vector_info_graph[d], self.method.vector_info_graph[indice_pivot]

                liste_branche[indice_pivot], liste_branche[d] = liste_branche[d], liste_branche[indice_pivot]
                return d
