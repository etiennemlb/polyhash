import os


class tokenizer(object):
    """[summary]
            Generate token out of an inputed file. This file must respect the
            standard described in the "hashcode2019_final_task.pdf" file            
       
        Example with a_example.in : 
            >>>token = tokenizer("C:\\Users\\Louis\\Desktop\\Gitlab\\polyhash-team-l\\test\\final_round_2019.in\\a_example.in")
            >>>token.tokenize()
            >>>print(token.file)
            "C:\\Users\\Louis\\Desktop\\Gitlab\\polyhash-team-l\\test\\final_round_2019.in\\a_example.in"

            >>>print(token.files)
            {'c0': [15, 5, [0]], 'c1': [10, 18, [0]], 'c2': [15, 35, [1, 'c0']], 
            'c3': [13, 52, [1, 'c1']], 'c4': [20, 52, [2, 'c1', 'c2']], 'c5': [15, 21, [2, 'c2', 'c3']]}  

            >>>token.extrem_files()
            >>>print(token.extrem_files)
            {'c4': [20, 52, [2, 'c1', 'c2']], 'c5': [15, 21, [2, 'c2', 'c3']]}
    """
    def __init__(self, file):
        """[summary]
            Tokenizer constructor
        
        Arguments:
            file {str} -- a string representing the input file
        """
        # could have used arrays but some test case require handling string
        # we are thus using a map (though it could be prevented using sweet hashing trickery)
        self.targetTokens = {}
        self.files = {}

        self.fileCount = 0
        self.targetCount = 0
        self.serverCount = 0

        self.max_score_without_bonus = None

        self.file = file

        self.extrem_files = {}

    def tokenize(self):
        """[summary]
            Basic token generation method
            Produce a map containing all the files and their dependency
            Produce a map containing the targets file
            Closes the file at the end of the tokenisation
       
        Raises:
            Exception: If the object as been cleaned up
        """
        # if i/o error -> let the exeption propagate
        input_file_fd = open(self.file, 'r')

        header = self.parseLine(
            input_file_fd.readline())

        self.fileCount = int(header[0])
        self.targetCount = int(header[1])
        self.serverCount = int(header[2])

        for file in range(self.fileCount):
            [fileName, compileTime, replicateTime] = self.parseLine(
                input_file_fd.readline())

            dependencies = self.parseLine(
                input_file_fd.readline())
            dependencies[0] = int(dependencies[0])

            self.files[fileName] = [
                int(compileTime), int(replicateTime), dependencies]

        for file in range(self.targetCount):
            [fileName, deadline, points] = self.parseLine(
                input_file_fd.readline())

            self.targetTokens[fileName] = [int(deadline), int(points)]

        # print(self.files)
        # print(self.targetTokens)
        input_file_fd.close()

        self.max_score_without_bonus = self.get_max_score()

    def isValidChar(self, c):
        """[summary]
            Check if a character is in a specific range :
            ('0' <= c and c <= '9') or ('a' <= c and c <= 'z')

        Arguments:
            c {char} -- A character

        Returns:
            [Bool]
        """
        return ('0' <= c and c <= '9') or ('a' <= c and c <= 'z')

    def parseLine(self, line):
        """[summary]
            Parse a line to generate tokens

        Arguments:
            file {str} -- A line to parse given it follows the google hash code 
            2019 syntax

        Example:
            >>>self.parsline("c0 15 5")
            ['c0','15','5']

        """
        items = ['']
        for c in line:
            if c == ' ':
                items.append('')
            elif self.isValidChar(c):
                items[-1] += c
            elif c == '\n':
                break
            else:
                # \n or other line ending chars, or unknown chars that shouldn't be here
                raise Exception('Invalid character found')
        return items

    def get_max_score(self):
        """[summary]
            Naive function which provide an approximate upper bound of the 
            amount of point
        Returns:
            int -- approximate score
        """
        if self.max_score_without_bonus is not None:
            return self.max_score_without_bonus
        total = 0
        for val in self.targetTokens.values():
            total += val[1]

        self.max_score_without_bonus = total
        return self.max_score_without_bonus

    def search_extrem_files(self):
        """[summary]
            Method used to search files that aren't used for any other files
            For example in a_example.in 
            C4 and c5 are extrem files

        :UC: this method have to be used after tokenize
        """
        if self.files != {}:
            for file1_key in self.files:
                found = False
                i = 0
                while not found and i < len(self.files):
                    for file2_key in self.files:
                        # check if the file1 is in the dependency of file2
                        if file1_key in self.files[file2_key][2]:
                            found = True
                        i += 1
                if not found:
                    self.extrem_files[file1_key] = self.files[file1_key]
            return self.extrem_files

    def info_about(self, file_name):
        """[summary]
            Give the info about a file 

            :param file_name: a file 
            :type file_name: str

            :return: return the list of the info of the file or 0 if the file is not in self.files
            :rtype: list

            :example: 
            file = {'c0': [15, 5, [0]], 'c1': [10, 18, [0]], 'c2': [15, 35, [1, 'c0']],
                        'c3': [13, 52, [1, 'c1']], 'c4': [20, 52, [2, 'c1', 'c2']], 
                        'c5': [15, 21, [2, 'c2', 'c3']]}

            >>>file.info_about('c0')
            [15, 5, [0]]
            compile, repl, depend
        """

        return self.files[file_name]
