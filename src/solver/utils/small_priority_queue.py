import heapq


class small_priority_queue(object):
    """[summary]
        Implemented using a min heap
        If a max heap is needed, multiply your
        priority by -1

        No sort stability in this implementation
    """

    def __init__(self):
        self.heap = []

    def is_empty(self):
        """[summary]
            Return the emptiness state of the queue
        Returns:
            boolean -- True if empty, else False
        """
        return len(self.heap) <= 0

    def push_heap(self, value, priority):
        """[summary]
            Add a value to the heap with a given priority
        Arguments:
            value {object} -- The value to push
            priority {comparable objects(int for instance)} -- The priority
        """
        heapq.heappush(self.heap, [priority, value])

    def pop_heap(self):
        """[summary]
            Returns the most prioritised element
        Returns:
            object -- The value popped
        """
        return heapq.heappop(self.heap)

    def top(self):
        """[summary]
            Peek at the value that would be returned by a pop_heap()
        Returns:
            object -- The value that has the most priority
        """
        return self.heap[0]

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return str(self.heap)
