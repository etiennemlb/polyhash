
def compute_reversed_dependency_graph(graph):
    """[summary]
        Produce a graph with reversed edge
        E.I:
            A -> B <- C
            Becomes:
            A <- B -> C
    Arguments:
        graph {dict} -- A dictionary representing a graph
            For instance :
            {a: [x], b: [y]}
            Here vertex a has dependencies represented by [x] and b by [y]
            [x] and [y] are list following the pattern of the tokenizer
    Returns:
        same input type -- The reversed graph
    """
    new_graph = {}
    for key in graph.keys():
        # make sure that a node has an entry in the map, even if it does not
        # have any dependencies
        new_graph[key] = new_graph.get(
            key, [graph[key][0], graph[key][1], [0]])

        for val_idx in range(1, graph[key][2][0]+1):
            val = graph[key][2][val_idx]
            # create the entry if needed
            new_graph[val] = new_graph.get(
                val, [graph[val][0], graph[val][1], [0]])

            new_graph[val][2].append(key)
            new_graph[val][2][0] += 1
    return new_graph
