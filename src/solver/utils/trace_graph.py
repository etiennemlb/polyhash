def trace(tokenized_file, graphe):
    """
    path = '/mnt/c/Users/louis/Desktop/Gitlab/polyhash-team-l/files/'
    file= 'a_example.in'
    l=louis(path+file)
    l.init()
    rep=utils.trace(l.tokenizer,l.graph)
    with open(file+'.dot','w') as f:
        f.write(rep)

    path = '/mnt/c/Users/louis/Desktop/Gitlab/polyhash-team-l/files/'
    file= 'f_big.in'
    l=louis(path+file)
    l.init()

    rep=utils.trace(l.tokenizer,[l.graph[0], l.graph[1][0:4] ])
    i='test'
    with open(file+'[' + str(i) + ']' +'.dot','w') as f:
        f.write(rep)
    """
    token_files = tokenized_file.files
    i = 0
    res = ""
    res += "digraph {\n"

    # for files in token_files:
    #     res += str(files)
    #     res += " [label="
    #     res += str(files)
    #     res += "]"
    #     if files in tokenized_file.targetTokens.keys():
    #         res += "[style=filled,color=pink]"
    #     res += "\n"

    #     i += 1

    res += str(__trace(tokenized_file, graphe))
    res += '}'


    return res



def __trace(tokenized_file, graphe, res=''):
    sub_branches = graphe[1]

    if sub_branches != []:
        for branches in sub_branches:
            string = ''
            if graphe[0] != 'root':
                if graphe[0] in tokenized_file.targetTokens:
                    string = str(graphe[0]) + " [label=" + str(graphe[0]) + ']' + '[style=filled,color=pink] \n' + str(graphe[0])+" -> "+str(branches[0])+'\n'
                else:
                    string = str(graphe[0])+" -> "+str(branches[0])+'\n'
            res += string + __trace(tokenized_file,branches)
        return res
    else:
        return ''
