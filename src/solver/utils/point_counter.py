from . import tokenizer
from . import small_priority_queue


class point_counter(object):
    """[summary]
        Given an output file conforming the standard described in the 
        "hashcode2019_final_task.pdf" file, Produce a series of simulation to 
        attempt to compute the amount of point that would be represented by a
        given solution.
    """
    def __init__(self, tokenized_file: tokenizer, result_file):
        """[summary]
            Constructor for the point counter
            call .load() to read the result_file
            call .compute() to to the simulation
        
        Arguments:
            tokenized_file {tokenizer} -- a tokenizer representing the file from
            which the solution has been produced
            result_file {str} -- the file representing the solution
        """
        # if i/o error -> let the exeption propagate
        self.result_file = result_file

        self.tokenized_file = tokenized_file

        self.compile_order_file = []
        self.compile_order_server = []

        self.pending_count = 0
        self.pending_compile = {}

        self.available_dependencies = {}
        self.server_current_time = {}

        self.points = 0

    def load(self):
        """[summary]
            Loads the result_file
        """
        result_file_fd = open(self.result_file, 'r')

        header = self.parseLine(result_file_fd.readline())
        self.compile_line_count = int(header[0])

        for line in range(self.compile_line_count):
            compile_order = self.parseLine(result_file_fd.readline())
            self.compile_order_file.append(compile_order[0])
            self.compile_order_server.append(int(compile_order[1]))

        result_file_fd.close()

    def is_dep_available(self, dep, local_server_id):
        """[summary]
            Check if a file has been built.
      
        Arguments:
            dep {str} -- string representing a file
            local_server_id {[type]} -- the server we are working on. This is 
            important because the time isn't computed the same way if a file 
            need to be replicated or not
        
        Returns:
            If is_dep_available: return [True,  earliest time when compiled and available]
            else:                return [False, None]
        """
        earliest_dep_compile_time = None

        for server, available_deps in self.available_dependencies.items():
            if dep in available_deps:
                # if the dependency is present in the local
                # server, no need to check a timestamp

                if earliest_dep_compile_time is None:
                    earliest_dep_compile_time = available_deps[dep][1]

                if server == local_server_id:
                    # Differentiating between cases is necessary
                    earliest_dep_compile_time = min(
                        earliest_dep_compile_time, self.server_current_time[local_server_id])
                    # breaking here could optimize
                else:
                    earliest_dep_compile_time = min(
                        earliest_dep_compile_time, available_deps[dep][1])

        return [earliest_dep_compile_time is not None, earliest_dep_compile_time]

    def are_deps_available(self, file, local_server_id):
        """[summary]
            Check if all dependencies of a given file are available.
       
        Arguments:
            file {str} -- string representing a file
            local_server_id {[type]} -- the server we are working on. This is 
            important because the time isn't computed the same way if a file 
            need to be replicated or not
        
        Returns:
            If are_deps_available: [True, last_dep_compile_time]
            else:                  [False, -1]
        """
        dependencies = self.tokenized_file.files[file][2]
        last_dep_compile_time = 0
        for i in range(1, dependencies[0]+1):
            dep = dependencies[i]
            found, time_when_available = self.is_dep_available(
                dep, local_server_id)
            # print(dep, 'is available:', found, time_when_available)
            if not found:
                return False, -1

            # Keep only the latest compiled dependency
            last_dep_compile_time = max(
                last_dep_compile_time, time_when_available)

        return True, last_dep_compile_time

    def try_compile(self):
        """[summary]
            Try to compile the solution and compute the score associated.
       
        Raises:
            Exception: If a starvation is detected
        """
        can_compile = True
        starving_level = 0
        unending_starving_level = 0

        while can_compile:
            if starving_level < unending_starving_level:
                raise Exception('Starvation detected')
            can_compile = False
            for server, server_compile_queue in self.pending_compile.items():
                has_compilable_file = True
                # while not empty or a file can be compiled
                while not server_compile_queue.is_empty() and has_compilable_file:

                    file_waiting_for_compile = server_compile_queue.top()[1]
                    # print(file_waiting_for_compile,'->',  server)

                    file_is_compilable, last_dep_compile_time = self.are_deps_available(
                        file_waiting_for_compile, server)
                    if file_is_compilable:
                        start_time = max(
                            self.server_current_time[server], last_dep_compile_time)
                        time_when_compiled = start_time + \
                            self.tokenized_file.files[file_waiting_for_compile][0]
                        time_when_replicated = time_when_compiled + \
                            self.tokenized_file.files[file_waiting_for_compile][1]

                        self.server_current_time[server] = time_when_compiled
                        self.available_dependencies[server][file_waiting_for_compile] = [
                            time_when_compiled, time_when_replicated]
                        # print('\tCompiled at', time_when_compiled, 'Replicated at', time_when_replicated)

                        self.compute_target_value(
                            file_waiting_for_compile, time_when_compiled)

                        # remvoe from the waiting queue
                        server_compile_queue.pop_heap()
                    else:
                        has_compilable_file = False

            for server, server_compile_queue in self.pending_compile.items():

                if not server_compile_queue.is_empty():
                    file_waiting_for_compile = server_compile_queue.top()[1]
                    file_is_compilable, last_dep_compile_time = self.are_deps_available(file_waiting_for_compile, server)
                    if file_is_compilable:
                        can_compile = True
                        starving_level += 1
                        break
            unending_starving_level += 1

    def compute_target_value(self, file, timestamp):
        """[summary]
            Given a target file and a timestamp, check if points have been 
            earned
        
        Arguments:
            file {str} -- string representing a file
            timestamp {int} -- time when the file was compiled
        """
        # if file is indeed a target
        if file in self.tokenized_file.targetTokens:
            target_goals = self.tokenized_file.targetTokens[file]
            if timestamp <= target_goals[0]:
                self.points += target_goals[1]  # goal points
                self.points += (target_goals[0] - timestamp)  # speed points
                print('\t->Target', file, 'hit for',
                      target_goals[1], 'and', target_goals[0] - timestamp, 'bonus pts')
            else:
                print('\t<-Missed target', file, 'dl by',
                      target_goals[0] - timestamp, 'minus', target_goals[1], 'pts')

    def compute(self):
        """[summary]
            Compute the score
        """
        self.load()
        for i in range(self.tokenized_file.serverCount):
            self.pending_compile[i] = small_priority_queue.small_priority_queue(
            )
            self.server_current_time[i] = 0
            self.available_dependencies[i] = {}

        for i in range(self.compile_line_count):
            # a file at a time
            local_server_id = self.compile_order_server[i]
            current_file = self.compile_order_file[i]

            self.pending_compile[local_server_id].push_heap(
                current_file, self.pending_count)
            self.pending_count += 1

        # print(self.pending_compile)
        self.try_compile()
        # print(self.server_current_time)
        # print(self.pending_compile)
        return self.points

    def isValidChar(self, c):
        """[summary]
            Check if a character is in a specific range :
            ('0' <= c and c <= '9') or ('a' <= c and c <= 'z')
      
        Arguments:
            c {[char]} -- A character

        Returns:
            [Bool]
        """
        return ('0' <= c and c <= '9') or ('a' <= c and c <= 'z')

    def parseLine(self, line):
        """[summary]
            Parse a line to generate tokens

        Arguments:
            file {str} -- A line to parse given it follows the google hash code 
            2019 syntax
        """
        items = ['']
        for c in line:
            if c == ' ':
                items.append('')
            elif self.isValidChar(c):
                items[-1] += c
            elif c == '\n':
                break
            else:
                # \n or other line ending chars, or unknown chars that shouldn't be here
                raise Exception('Invalid character found')
        return items
