class serializer(object):
    """[summary]
        Serialize a solution
    """
    def __init__(self, file, method):
        """[summary]
            Constructor of the serializer
            call .append() to add a line to the solution
            call .serialize() to serialize to a file
        Arguments:
            file {str} -- string representing the output file
            method {str} -- wich method was used to compute the solution
        """
        self.file = file
        self.orders = []
        self.method = method

    def append(self, order: list):
        """[summary]
            Append a line to the solution to be serialized
        Arguments:
            order {list} -- a liste containing [file_to_be_compiled, on_server_x]
        """
        self.orders.append(order)

    def serialize(self):
        """[summary]
            Overwrite the output file and write the solution
        """
        output_file_fd = open(self.file + '.' + self.method + '.out', 'w')
        output_file_fd.truncate(0)

        output_file_fd.write(str(len(self.orders)))
        output_file_fd.write('\n')

        for order in self.orders:
            output_file_fd.write(str(order[0]))
            output_file_fd.write(' ')
            output_file_fd.write(str(order[1]))
            output_file_fd.write('\n')

        output_file_fd.close()
