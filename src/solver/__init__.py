__version__ = "0.0.1"

from .solver import solve
from .solver import getMethodsList
from .solver import count_points
from .solver import solve_and_count
