import sys
import copy
import operator

from . import utils


class base_method(object):
    def __init__(self, file):
        """Basic interface, generate a tokenizer object.
        Data modelisation is not handled.

        Arguments:
            file {string} -- A path to a valid input file representing the problem
        """
        self.file = file
        self.tokenizer = utils.tokenizer(self.file)

    def init(self):
        self.tokenizer.tokenize()

    def solve(self):
        pass

    def get_result(self, serializer):
        pass


class naive_method(base_method):
    def __init__(self, file):
        super(naive_method, self).__init__(file)

    def solve(self):
        print(utils.compute_reversed_dependency_graph(self.tokenizer.files))


class louis(base_method):
    
    def __init__(self, file):

        super(louis, self).__init__(file)
        sys.setrecursionlimit(20000000)

    def init(self):
        """
        Initialization of resolution method
        First, we have to generate extrem files
        From them, we generate the graph with the get_graph() method
        """
        super(louis, self).init()
        print(' Searching extrem files')
        self.tokenizer.search_extrem_files()
        print(' Done')
        print(' Getting graph')
        self.graph, self.vector_info_graph = self.get_graph()
        print(' Done')
        
    def solve(self):
       
        print(' Sorting graph')
        utils.Sort(self,'ratio','<') 
        utils.Sort(self,'remove_extra')   
        utils.Sort(self,'not_worth') 
        print(' Done')
        
        print(' Using : ', len(self.graph[1]), 'servers on', self.tokenizer.serverCount)

        self.free_serv = [self.tokenizer.serverCount - len(self.graph[1]), len(self.graph[1]),self.tokenizer.serverCount]
                            #nb_serv_free                                   indice de separation       nb serv total


        
        if self.free_serv[0] > 0: # if they are free serv
            print(' Dispatching on free servers')
            self.coeff_dispatch = (self.free_serv[0]/self.free_serv[2])
            utils.Sort(self,'dispatch')
            print(' Done')


        self.vector_serv = []
        i = 0
        while len(self.vector_serv) != len(self.graph[1]):
            if i == self.tokenizer.serverCount:
                i = 0
            self.vector_serv.append(i)
            i += 1

        self.original_size = min(len(self.graph[1]),self.tokenizer.serverCount) # Used in __solve

        self.solved = []
        
        print(' Generation of a result')
        while self.graph[1] != []:
            self.__solve()
        print(' Done')

        # This part remove files that don't need to be compile thanks to the replication
        print(' Removing duplicated files from result')
        self.remove_trash(self.solved)
        print(' Done')

    def __solve(self):
        """
        This method generate the result : it take file at the bottom left of each connex graph
        """
        i = 0
        while i < self.original_size:
            num_serv = self.vector_serv[i]
            branches = self.graph[1][i]
            if branches[1] == []:
                if [str(branches[0]), str(num_serv)] not in self.solved:  # enleve doublons
                    self.solved.append([str(branches[0]), str(num_serv)])

                found = False
                j = i+1
                if len(self.vector_serv) <= self.original_size:
                    self.original_size -= 1

                while not found and j < len(self.graph[1]):
                    search_num_serv = self.vector_serv[j]
                    if search_num_serv == num_serv:
                        found = True
                        self.vector_serv.pop(j)
                        self.graph[1][i] = self.graph[1].pop(j)
                    j += 1
                if not found:
                    self.vector_serv.pop(i)
                    self.graph[1].pop(i)
                    i -= 1

            else:
                while branches[1] != []:
                    branches, prev = branches[1][0], branches
                if [(branches[0]), str(num_serv)] not in self.solved:  # enleve doublons
                    self.solved.append([str(branches[0]), str(num_serv)])

                prev[1].pop(0)

            i += 1

    def get_graph(self):
        """
        Generate the graph 

        :return: treemap,vector_info
        :example: example of vector info: [
         [14589,          16191,       [14689,     28160,      100,          100]],    [.,.[.,.,.,.]],  ...]
        [compil branche, repl branche[deadline, score targ, compil targ, repl targ]

        """
        treemap = ['root', []]
        vector_info = []
        
        for fich in self.tokenizer.extrem_files :
            stack=[]
            if fich in self.tokenizer.targetTokens:
                dict_info = {}
                info_target = self.tokenizer.targetTokens[fich]
                info_fich = self.tokenizer.info_about(fich)
                if info_fich[0] < info_target[0]:
                    #compile time < deadline
                    info_branche = self.__get_connex(self.tokenizer.extrem_files[fich],[0,0],stack,dict_info)
                    #for the target
                    #suppress impossible cases 
                    if (info_branche[1][0])/self.tokenizer.serverCount <= info_target[0]:
                        info_branche[1].append(info_target+[info_fich[0]]+[info_fich[1]])
                        #add to info connex_graph     deadline,score,tempscompil,replic
                        
                        treemap[1].append([fich, info_branche[0]])
                        vector_info.append(info_branche[1]) 

        return treemap,vector_info


    def __get_connex(self, info_file,comp_repl,stack,dict_info ):
        """
        get a connex graph from the global graph
        """

        dependencies = info_file[2][1:]
        graph = []
        
        # dependences compil repl 
        for files in dependencies:
            info = self.tokenizer.info_about(files)
            dict_info[files] = info
            sub = self.__get_connex(info,comp_repl,stack,dict_info )
            graph.append([files, sub[0]])
            if files not in stack:
                comp_repl[0] += info[0]
                comp_repl[1] += info[1]
                stack.append(files)

        return [graph,comp_repl,dict_info ]


    def remove_trash(self, res):
        """
        remove # from the result
        """
        i = 0
        while i < len(res):
            fich=res[i]
            if fich[0] == '#': # caused by the dispatch
                res.pop(i)
                i-=1
            i += 1
        
        
    def get_result(self, serializer):
        for ligne in self.solved:
            serializer.append(ligne)


class etienne(base_method):
    def __init__(self, file):
        super(etienne, self).__init__(file)

        # max heap
        self.ready = utils.small_priority_queue()
        self.executed = {}

        self.servers_completed_time = []

        self.server_of_task = {}
        self.start_time_of_task = {}
        self.replication_end_time = {}

        self.reversed_graph = None
        self.b_level = {}

    def init(self):
        super(etienne, self).init()

    def argmin(self, list: dict):
        min_key = None
        min_val = None

        for key, val in list.items():
            if min_key is None:
                min_key = key
                min_val = val
            if val < min_val:
                min_key = key
                min_val = val

        return min_key, min_val

    def set_bottom_level_priorities(self):
        """[summary]
        A b-level(bottom level) can be defined recursively as :

        b_level(ni) = wi + (0 if no succ
                            max(cost(ni, ny) + b_level(ny))
                            where ny in succ(ni)
                            )
        In our case wi + cost(ni, ny) invariant thus the max 
        can only be done on the b_level of the children
        TODO :
        - check invariant wi + cost(ni, ny) optimisation
        """
        # for all vertex
        for n_i in self.reversed_graph.keys():
            self._set_bottom_level_priorities(n_i)

    def _set_bottom_level_priorities(self, n_i):
        n_i_info = self.reversed_graph[n_i]

        # once a b_level is computed, no need to compute it again
        if n_i in self.b_level:
            return self.b_level[n_i]

        # if no successors, 0 b-level
        max_b_level = 0

        # for all successors/child's
        for n_y_idx in range(1, n_i_info[2][0] + 1):
            n_y = n_i_info[2][n_y_idx]

            # if cost(ny, ni) + b-level(ni) > max
            # call to recursive _set_bottom_level_priorities
            max_b_level = max(
                max_b_level, n_i_info[1] + self._set_bottom_level_priorities(n_y))

        # blevel(ni) = max + weight(ni)
        n_i_b_level = max_b_level + n_i_info[0]

        if n_i in self.tokenizer.targetTokens:
            """
            self.tokenizer.targetTokens[n_i][X]
            where X = 0 represent the deadline of a target
            and   X = 1 represent the amout of pts earned by meeting the deadline on the target n_i

            Know your data, optimize for it, thus:

            a best: n_i_b_level += 0

            b best: n_i_b_level += self.tokenizer.targetTokens[n_i][0]*11.54

            c best: n_i_b_level += self.tokenizer.targetTokens[n_i][0] + 1 / self.tokenizer.targetTokens[n_i][1]

            d best: n_i_b_level += self.tokenizer.targetTokens[n_i][0]*10

            e best: n_i_b_level += 10000000000 * 1 / self.tokenizer.targetTokens[n_i][0]

            f best: n_i_b_level += self.tokenizer.targetTokens[n_i][1] + self.tokenizer.targetTokens[n_i][0]
            """
            pass # n_i_b_level += self.tokenizer.targetTokens[n_i][0]*10

        self.b_level[n_i] = n_i_b_level
        return n_i_b_level

    def initialize_ready_task(self):
        # for each vertex
        for vertex, vertex_info in self.tokenizer.files.items():
            # if it has no pred
            if vertex_info[2][0] == 0:
                self.ready.push_heap(vertex, -self.b_level[vertex])

    def initialize_ready_task_for_specific_target(self, target_list):

        seen_dependencies = {}
        dependency_list = target_list
        dependencies = None

        for target in target_list:
            if self.tokenizer.files[target][2][0] == 0:
                self.ready.push_heap(target, -self.b_level[target])

        dep_idx = 0
        while dep_idx < len(dependency_list):
            dependencies = self.tokenizer.files[dependency_list[dep_idx]][2]

            for curr_dep_idx in range(1, dependencies[0] + 1):
                dep = dependencies[curr_dep_idx]

                if dep not in seen_dependencies:
                    seen_dependencies[dep] = 1
                    dependency_list.append(dep)
                    if self.tokenizer.files[dep][2][0] == 0:
                        self.ready.push_heap(dep, -self.b_level[dep])
            dep_idx += 1

    def prune_unnecessary_files(self):
        """[summary]
        Removes files that do not have any successor and that are not a target
        """
        print('\t\tBuilding the reversed graph')
        self.reversed_graph = utils.compute_reversed_dependency_graph(
            self.tokenizer.files)

        iterable_vertex_list = []
        for vertex in self.reversed_graph.keys():
            iterable_vertex_list.append(vertex)

        might_be_able_to_remove = True
        while might_be_able_to_remove:
            might_be_able_to_remove = False

            for vertex in iterable_vertex_list:
                # if not a target and has no dependencies (no future use)
                if vertex not in self.tokenizer.targetTokens and self.reversed_graph[vertex][2][0] == 0:

                    # removing dep from parent of vertex in reversed
                    for vertex_parent_idx in range(1, self.tokenizer.files[vertex][2][0] + 1):
                        vertex_parent = self.tokenizer.files[vertex][2][vertex_parent_idx]

                        # slow, might have to sort
                        self.reversed_graph[vertex_parent][2].remove(vertex)
                        self.reversed_graph[vertex_parent][2][0] -= 1

                    del self.tokenizer.files[vertex]
                    iterable_vertex_list.remove(vertex)
                    # print('\t\tRemoved', vertex)

                    might_be_able_to_remove = True

    def prune_files_not_matching_deadline(self, level=0):
        """[summary]
        Remove heuristicaly, the files that whould prevent meeting a deadline
        """       
        target_time_branch = {}
        
        uncompilable_targets = []
        
        for target in self.tokenizer.targetTokens.keys():
            target_time_branch[target] = 0

            dep_idx = 0
            dependency_list = [target]
            seen_dependencies = {target: 1}
            
            depth = 0
            while dep_idx < len(dependency_list) and depth <= level:
                file_name = dependency_list[dep_idx]
                dependencies = self.tokenizer.files[file_name][2]

                target_time_branch[target] += self.tokenizer.files[file_name][0] 
                # print('on', file_name)
                # print(dep_idx)
                # print('cost is', target_time_branch[target])
                # if compiling a target's dependencies takes more time than the fixed deadline
                if target_time_branch[target] > self.tokenizer.targetTokens[target][0]:  # todo better heuristic
                    uncompilable_targets.append(dependency_list[0])
                    break

                for curr_dep_idx in range(1, dependencies[0] + 1):
                    dep = dependencies[curr_dep_idx]

                    times_saw = seen_dependencies.get(dep, 0) + 1
                    seen_dependencies[dep] = times_saw
                    if times_saw > 0:
                        dependency_list.append(dep)

                dep_idx += 1
                depth += 1

        for invalid_target in uncompilable_targets:
            del self.tokenizer.targetTokens[invalid_target]

    def finish_time(self, a):
        return (self.start_time_of_task[a] + self.reversed_graph[a][0])

    def BL_EST(self, specific_targets=None):
        print('\tComputing the graph BL')
        self.set_bottom_level_priorities()

        print('\tSearching for dependency less files')
        if specific_targets is not None:
            self.initialize_ready_task_for_specific_target(specific_targets)
        else:
            self.initialize_ready_task()

        print('\tSolving')

        for k in range(self.tokenizer.serverCount):
            self.servers_completed_time.append(0)

        while not self.ready.is_empty():

            b_level, v_i = self.ready.pop_heap()
            # print('working on', v_i)
            pred_v_i = self.tokenizer.files[v_i][2][1:]

            earliest_server_available = {}
            # Update earliest possible begin time begin[k](earliest_server_available) with
            # the latest finishing predecessor communication.
            for k in range(self.tokenizer.serverCount):
                # earliest time is now, asa the server is freed
                earliest_server_available[k] = self.servers_completed_time[k]

                for v_j in pred_v_i:
                    # when the file was compiled (no replication)
                    # always at least the compilation end time
                    lastest_pred_finished_time = self.start_time_of_task[v_j] + \
                        self.reversed_graph[v_j][0]

                    # µ(j) = self.server_of_task[v_j]
                    server_where_v_j_was_processed = self.server_of_task[v_j]
                    # if this pred was compiled on the same server
                    if server_where_v_j_was_processed == k:
                        # time when the pred finished being compiled
                        # print(v_j, 'is local')
                        pass
                    else:
                        # TODO, possibly compile on 2 servers, but it'll break start time of task

                        # time of replication + time when the pred finished being compiled
                        lastest_pred_finished_time += self.reversed_graph[v_j][1]

                    earliest_server_available[k] = max(
                        earliest_server_available[k], lastest_pred_finished_time)

            # best server ( where we can start asap)
            k_best, _ = self.argmin(earliest_server_available)
            self.server_of_task[v_i] = k_best
            # print('choosing', k_best)

            # The task start when the server is freed (by default). Not always true thus:
            self.start_time_of_task[v_i] = self.servers_completed_time[k_best]

            for v_j in pred_v_i:
                # This time depends on the last replication end time of a pred
                start_time = self.start_time_of_task[v_j] + \
                    self.reversed_graph[v_j][0]

                # µ(j) = self.server_of_task[v_j]
                server_where_v_j_was_processed = self.server_of_task[v_j]
                # if this pred was compiled on the same server
                if server_where_v_j_was_processed == k_best:
                    # compiled localy, thus no communicaton needed
                    pass
                else:
                    # compiled localy on an other server then replicated
                    start_time += self.reversed_graph[v_j][1]

                self.start_time_of_task[v_i] = max(
                    self.start_time_of_task[v_i], start_time)

            # Server k_best next availability
            self.servers_completed_time[k_best] = self.start_time_of_task[v_i] + \
                self.reversed_graph[v_i][0]
            # Add replication end time
            self.replication_end_time[v_i] = self.servers_completed_time[k_best] + \
                self.reversed_graph[v_i][1]
            # Add to Ex
            self.executed[v_i] = 1

            # Find next ready task
            for succ_idx in range(1, self.reversed_graph[v_i][2][0] + 1):
                succ = self.reversed_graph[v_i][2][succ_idx]

                # if already executed dont add
                # if its a file that is not a target and dont have any succ, this case should be
                # taken care of by the pruning phase
                if succ in self.executed:
                    continue
                else:
                    all_pred_of_succ_processed = True
                    for pred_of_succ_idx in range(1, self.tokenizer.files[succ][2][0] + 1):
                        pred_of_succ = self.tokenizer.files[succ][2][pred_of_succ_idx]

                        if pred_of_succ not in self.executed:
                            all_pred_of_succ_processed = False
                            break
                    if all_pred_of_succ_processed:
                        self.ready.push_heap(succ, -self.b_level[succ])

    def solve(self):#, i):
        print('\tPruning uncompilable files')
        # mostly necessary for f_big.in
        self.prune_files_not_matching_deadline(0)

        print('\tPruning the graph from unnecessary files')
        self.prune_unnecessary_files()

        print('\tRe-Building the reversed graph')
        self.reversed_graph = utils.compute_reversed_dependency_graph(
            self.tokenizer.files)

        target_list = []
        for target in self.tokenizer.targetTokens.keys():
            target_list.append(target)
        
        # best targets for c_urgent: 1386790 pts or 51.54% of max without bonus
        # target_list = ['cyfl', 'chbl', 'cmyl', 'c3w6', 'ctk6']
        
        self.BL_EST(target_list) #[target_list[i]])

    def get_result(self, serializer):
        sorted_times = sorted(
            self.start_time_of_task.items(), key=operator.itemgetter(1))
        
        for vertex, _ in sorted_times:
            serializer.append([vertex, str(self.server_of_task[vertex])])


class etienne_naive(etienne):
    def __init__(self, file):
        super(etienne_naive, self).__init__(file)

    def naive_after_pruning_server_stuffing(self, specific_targets=None):
        print('\tSolving')

        servers_time = {}
        seen_dependencies = {}
        for target_idx, target in enumerate(specific_targets):       
            server_id = target_idx % self.tokenizer.serverCount

            # target used by an other target
            if self.reversed_graph[target][2][0] != 0:
                continue

            self.server_of_task[target] = [server_id]
            
            if server_id not in servers_time:
                servers_time[server_id] = 0
                seen_dependencies[server_id] = {}

            self.start_time_of_task[target] = [servers_time[server_id]]
            servers_time[server_id] -= self.tokenizer.files[target][0]
            
            dep_idx = 0
            dependency_list = [target]
            seen_dependencies[server_id][target] = 1

            # print('for', target, 'on', server_id)
            while dep_idx < len(dependency_list):
                file_name = dependency_list[dep_idx]
                           
                dependencies = self.tokenizer.files[file_name][2]
                for curr_dep_idx in range(1, dependencies[0] + 1):
                    dep = dependencies[curr_dep_idx]

                    if dep not in seen_dependencies[server_id]:
                        seen_dependencies[server_id][dep] = 1
                        dependency_list.append(dep)

                        if dep not in self.server_of_task:
                            self.server_of_task[dep] = []
                            self.start_time_of_task[dep] = []

                        self.server_of_task[dep].append(server_id)
                        self.start_time_of_task[dep].append(servers_time[server_id])
                        servers_time[server_id] -= self.tokenizer.files[dep][0]
                        #print(servers_time[server_id], dep)
                
                dep_idx += 1

    def solve(self):
        print('\tPruning uncompilable files')
        # mostly necessary for f_big.in
        self.prune_files_not_matching_deadline(0)

        print('\tPruning the graph from unnecessary files')
        self.prune_unnecessary_files()

        print('\tRe-Building the reversed graph')
        self.reversed_graph = utils.compute_reversed_dependency_graph(
            self.tokenizer.files)

        target_list = []
        for target in self.tokenizer.targetTokens.keys():
            target_list.append(target)

        self.naive_after_pruning_server_stuffing(target_list)

    def get_result(self, serializer):
        sorted_times = sorted(
            self.start_time_of_task.items(), key=operator.itemgetter(1))
     
        for vertex, _ in sorted_times:
            for server in self.server_of_task[vertex]:
                serializer.append([vertex, str(server)])


class tudual(base_method):
    def __init__(self, file):
        super(tudual, self).__init__(file)

    #Elementary function

    def summ(liste): # as we can't use sum()
        s = 0
        for e in liste:
            s += e
        return s

    def maxi(liste): # as we can't use max()
        m = liste[0]
        for e in liste:
            if e > m: m = e
        return m

    def maxii(liste): # as we can't use max()
        m = liste[0]
        for e in liste:
            for j in e:
                if j > m: m = j
        return m

    def mini(liste): # as we can't use min()
        m = liste[0]
        for e in liste:
            if e < m: m = e
        return m

    def inside(e, liste): # as we can't use in()
        for i in liste:
            if i == e: return True
        return False    

    def sort(liste): # as we can't use sorted() with lambda
        """Sort the liste by using quicksort by the second element."""

        less, equal, greater = [], [], []
        n = len(liste)
        if n > 1:
            pivot = liste[0][1]
            for i in range(n):
                if liste[i][1] < pivot:    less.append(liste[i])
                elif liste[i][1] == pivot: equal.append(liste[i])
                elif liste[i][1] > pivot:  greater.append(liste[i])
            return sort(less)+equal+sort(greater)
        else:
            return liste

# order does matter
# dont refactor it, only append
methods = ['naive', 'tudual', 'louis', 'etienne', 'etienne_naive']


def methodFactory(method, file):
    """[summary]
        Produce the appropriate object from a given class name
        This function needs to be updated to match the currently existing 
        methods

    Arguments:
        method {string} -- The class to be constructed, must be included into
        the methods list
        file {string} -- The only parameter required by the base class 
        base_method
    """
    if method not in methods:
        print('Could not find the method', method,
              ', falling back to naive_method!\n')
        return naive_method(file)

    obj = None

    if method == methods[0]:
        obj = naive_method(file)
    elif method == methods[1]:
        obj = tudual(file)
    elif method == methods[2]:
        obj = louis(file)
    elif method == methods[3]:
        obj = etienne(file)
    elif method == methods[4]:
        obj = etienne_naive(file)
    # append to the elif statement when to create a new method, remember to add it to the list, "methods"

    print('Using', method, '\'s method')

    return obj
