import argparse

import solver

__version__ = '0.0.1'


def formatRawFilesArgs(rawArguments):
    """[summary]
        Convert a pententialy 2D list of string to a 1D list
        
    Arguments:
        rawArguments {list} -- a list of string with pententialy 2D
    
    Raises:
        TypeError: non comformant list
    
    Returns:
        list of string -- A proper 1D list of string
    """
    argList = []
    for e in rawArguments:
        if isinstance(e, ''.__class__):
            argList.append(e)
        elif isinstance(e, list):
            for f in e:
                argList.append(f)
        else:
            raise TypeError  # Not a list, string
    return argList


def main():
    """[summary]
        Entry function for the solver
    """
    parser = argparse.ArgumentParser('Solver du probleme googleHashCode 2019')

    parser.add_argument('-s', '--solve', help='Try to solve the problem, must set -f (--files too)',
                        action='store_true', default=False)

    parser.add_argument(
        '-c', '--count',  help='Counts the amount of points earned for given files, result file must match this pattern:\n[filepath & filename].out, given by -f (--files)', action='store_true', default=False)

    parser.add_argument(
        '-m', '--method',  help='The method used to solve the problem, to be used with -s (--solve), available methods: ' + str(solver.getMethodsList()), default=False)

    parser.add_argument('-V', '--version', action='version',
                        version='%(prog)s ' + __version__)

    mandatoryArgs = parser.add_argument_group('Mandatory arguments:')
    mandatoryArgs.add_argument(
        '-f', '--files', help='Describes the files the tool will work on', nargs='+', dest='files', action='append', default=[])

    args = parser.parse_args()

    if (args.solve or args.count) and len(args.files) > 0:
        files = formatRawFilesArgs(args.files)

        method = 'naive'

        # Can be False or a string
        if args.method != False:
            method = args.method

        if args.solve and args.count:
            solver.solve_and_count(files, method)
        elif args.solve:
            solver.solve(files, method)
        elif args.count:
            for file in files:
                solver.count_points(file, method)
    else:
        parser.print_help()


if __name__ == '__main__':
    main()
