# Projet Poly# 2019


### I - Équipe
---

Notre équipe est l'équipe L.  
Elle est composée des membres suivants :

* Étienne MALABOEUF   
* Tudual QUINIO  
* Louis ROUSSEL  
* Hadrien WILLEMOT  


### II - Descriptif du projet
---
Dans ce projet, nous avons une suite de fichiers qui doivent être compilés sur des serveurs. Un fichier est d'abord être compilé sur un serveur en un certain temps,
puis est recopié sur tous les autres serveurs en même temps, en une autre durée. Certains fichiers sont dépendants d'autres fichiers,
c'est-à-dire que ces fichiers doivent être présents au préalable sur un serveur (par compilation directe ou par recopie) pour que l'on puisse y compiler le fichier actuel.
Certains fichiers spéciaux appelés "target" sont des fichiers dont la compilation rapporte des points. Si ces targets ont fini d'être compilées avant la fin d'un temps limite (appelé "deadline") propre à chaque target,
la compilation rapporte en point l'équivalent du nombre de secondes qu'il restait avant que la deadline soit atteinte. De plus, que la deadline soit dépassée ou non, la compilation d'un target rapporte un nombre de points fixes liés à chaque target.

Le but du projet est, pour une liste de fichiers contenant targets et dépendances, renvoyer l'ordre des compilations que l'on effectuerait, sur quel serveur chaque compilation serait effectuée, dans le but de compiler tous les fichiers en gagnant le plus de points possibles.
Il faut également pouvoir créer un correcteur automatique qui va lire une solution proposée pour un groupe de fichiers à compiler, vérifier que toutes les compilations sont possibles (par exemple qu'un fichier 1 dépendant d'un autre fichier 2 ne soit pas envoyé à la compilation alors que le fichier 2 n'est pas encore présent sur le serveur choisi)
Enfin le correcteur doit pouvoir calculer, si la solution proposée est correcte, le nombre de points obtenus.


### III - Présentation des outils communs : 
---

**tokenizer():**  
La classe tokenizer permet d'associer à chaque fichier à compiler dans les fichiers .in toutes les informations nécessaires pour la résolution du problème, telle que:
* le nombre de fichiers existants
* le nombre de fichiers target
* le nombre de serveurs disponibles
* pour chaque fichier le temps de compilation, le temps de réplication, les fichiers dont il est dépendant (qui doivent avoir été compilé au préalable), et les deadlines et points pour compilation pour chaque target.

Il indique également quels sont les fichiers extrêmes (c'est-à-dire ceux qui ne dépendent d'aucun autre fichier)
Le résultat de tokenize est représenté sous la forme d'un dictionnaire python pour pouvoir accéder aux informations liées à un fichier en particulier plus facilement.

**base_method():**  
Utilise la classe tokenizer à partir d'un fichier pour initialiser les données de du problèmes (fichiers à compiler, target, etc.) de manière ordonnée et égale pour toutes les méthodes qui seront créées ensuite. 
base-méthode permet également ensuite d'appeler le solver à la fin de la méthode, afin de présenter les résultats plus facilement




### IV - Stratégies de résolution
---

#### A. Première stratégie : etienne  
**Method Etienne():**  
-D'abord, l'ensemble des fichiers et de leurs dépendances est représenté à l'aide de graphes orientés sans circuits, surnommés DAG.
-On supprime du DAG les fichiers qui n'ont aucun successeur (=qui ne dépendent pas de ce fichier) et qui ne sont pas des targets.
-On supprime également les fichiers prédécesseurs directs de la target qui ne peuvent dans tous les cas possibles être compilés avant leur deadline.

-Ensuite, on calcule pour chaque sommet restant du graphe son "poids"
(à l'aide de BL_EST, qui prend le temps de compilation du fichier + le maximum entre le temps de compilation et réplication du plus gros fichier prédécesseur et le poids cumulé des fichiers prédécesseurs.)
-On classe les fichiers par poids décroissant.

-On prend la liste des fichiers qui ne dépendent d'aucun autre fichier, triés par poids décroissants, et tant que cette liste n'est pas vide, on prend le sommet prioritaire et on recherche la position la plus avantageuse parmi les serveurs pour la compiler
Une fois compilées, les dépendances des fichiers évoluent, réalimentant la liste avec de nouveaux fichiers qui désormais peuvent être compilés.




#### B. Deuxième stratégie : louis  
**Method Louis():**  
Initialisation
-en premier lieu, on regarde tout les fichiers extrêmes, c'est-à-dire ceux dont aucun fichier ne dépend.
-À partir de chacun de ces fichiers extrêmes est généré un graphe orienté sans cycle, où chaque sommet a une hauteur 
(hauteur n: ne dépend d'aucun fichier, hauteur n-1: dépend d'un ou plusieurs fichiers de hauteur n, hauteur n-2: dépend d'un ou plusieurs fichiers de hauteur n-1, ...)

Sort(not_worth)
-On effectue ensuite un tri, où on retire les composantes connexes si le fichier de hauteur 0 de cette composante n'est pas une target.
-On retire également les composantes dont les sommets de hauteur 0 qui sont target ne peuvent pas être compilés à temps (dont la deadline est dépassée)

Sort(deadline)
-On tri les composantes connexes par deadline de target croissantes

-On répartie les composantes sur les serveurs de manière à ce qu'une composante correspondent à 1 serveur.
Pour ça il faut calculer le nombre de serveurs utilisés, et de serveurs libres.

-Pour dire qu'un groupe de fichiers (ensemble connexe du graphe) doit être dispatché sur tous les serveurs libres, on marque le serveur qui a été affilié à cet ensemble de fichiers d'un dièse

-Équilibrage et répartition des fichiers sur les serveurs :
Les serveurs qui doivent être dispatchés et les serveurs libres sont dans un dictionnaire sur python. Ainsi, si on a un serveur qui est symbolisé d'un dièse (c'est à dire dont les fichiers peuvent être dispatchés sur plusieurs serveurs),
alors on a dispatcher les fichiers du serveur ayant un dièse sur les serveurs libres.
(Attention, dispatcher n'est pas forcément la meilleure solution, car elle peut limiter le nombre de serveurs disponibles pour de futurs ensembles de fichiers)

Résultat : On génère les solutions, c'est à dire on prend les fichiers de hauteur n à la suite, qu'on associe à chaque serveur, puis on supprime de cette résolution les fichiers doublons qui par exemple avaient déjà été répliqués.



Limite de l'algo : dispatch : réduit la possibilité sur d et c.
                    En effet, pour ces fichiers, il est plus judicieuxd'utiliser la totalité des serveurs libres disponibles 




### V - Tableau de score
---

    
|        | a_example | b_narrow | c_urgent | d_typical | e_intriguing | f_big |
| ------ | ------     |------   |------    |------     |------        |------ |
| Etienne | 25        |1427675      |1404028      |1022520      |1            | 840971    |
| Louis | 0       | 1360659       |1295335        |3155        | 524288  | 840971   |
| Tudual | 0        |0    |25        |0        |0             |  0  | 

### VI - Graphes
---

<div align="center">

a_example  
---
<img src="https://gitlab.univ-nantes.fr/poly-team-l/polyhash-team-l/uploads/77199988bbd802b00083613470a9dcc2/a_example.in.sfdp.jpg" width="500">


b_narrow
---
<img src="https://gitlab.univ-nantes.fr/poly-team-l/polyhash-team-l/uploads/8bd1183934f00c7a189e94649aa3c080/Graphe_b_narrow.png" width="500">


c_urgent
---
<img src="https://gitlab.univ-nantes.fr/poly-team-l/polyhash-team-l/uploads/4eee003af6713796587f732656da9911/graphe_c_urgent.png" width="500">

d_typical
---
<img src="https://gitlab.univ-nantes.fr/poly-team-l/polyhash-team-l/uploads/b1dc71d2919e937ae75761ebaa60f24b/d_typical.in.sfdp.jpg" width="500">


e_intriguing
---
<img src="https://gitlab.univ-nantes.fr/poly-team-l/polyhash-team-l/uploads/c85146783f7486c8ccfa32b414e1e3fb/e_intriguing.in.dot.jpg" width="500">


f_big
---
<img src="https://gitlab.univ-nantes.fr/poly-team-l/polyhash-team-l/uploads/9c9c32fd5243a42f60ba8dfc6e9ec39a/f_big.in.dot.jpg" width="500">


Sous-graphe f_big
---
<img src="https://gitlab.univ-nantes.fr/poly-team-l/polyhash-team-l/uploads/1da2a9023b5c001ebba1e169a4fb8686/Sous_graphe_f_big.png" width="500">
</div>


### VII - Exécution du programme  
---

Pour lancer le programme de résolution, le fichier à exécuter est googleHashCodeSolver.py.
    
    usage: Solver du problème googleHashCode 2019 [-h] [-s] [-c] [-m METHOD] [-V]
                                                  [-f FILES [FILES ...]]
    
    optional arguments:
      -h, --help            show this help message and exit
      -s, --solve           Try to solve the problem, must set -f (--files too)
      -c, --count           Counts the amount of points earned for given files,
                            result file must match this pattern: [filepath &
                            filename].out, given by -f (--files)
      -m METHOD, --method METHOD
                            The method used to solve the problem, to be used with
                            -s (--solve), available methods: ['naive', 'tudual',
                            'louis', 'etienne']
      -V, --version         show program's version number and exit
    
    Mandatory arguments::
      -f FILES [FILES ...], --files FILES [FILES ...]
                            Describes the files the tool will work on



